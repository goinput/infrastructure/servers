######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

##############################
### Network: Nextcloud
##############################

resource "hcloud_network" "hcloud_network_nextcloud" {
  name     = "nextcloud-network"
  ip_range = "192.168.100.0/24"

  labels = {
    "service" = "nextcloud"
    "type"    = "network"
  }
}

resource "hcloud_network_subnet" "hcloud_network_nextcloud_subnet" {
  network_id   = hcloud_network.hcloud_network_nextcloud.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "192.168.100.0/24"

  depends_on = [
    hcloud_network.hcloud_network_nextcloud
  ]
}

##############################
### Nextcloud
##############################

resource "hcloud_server" "nextcloud" {
  count = 1
  name = format("%s-%s.%s",
    "nextcloud${count.index + 1}",
    (count.index % 2 == 0 ? "fsn1" : "nbg1"),
    "goitservers.com"
  )
  image = "debian-11"

  server_type = "cx21"
  location    = (count.index % 2 == 0 ? "fsn1" : "nbg1")

  backups  = false
  ssh_keys = data.hcloud_ssh_keys.hetzner_all_keys.ssh_keys.*.name
  labels = {
    service        = "nextcloud"
    terraform      = true
    placementgroup = true
  }
  placement_group_id = hcloud_placement_group.nextcloud_placement_group.id
}

resource "hcloud_server_network" "nextcloud_network" {
  count = length(hcloud_server.nextcloud)

  server_id  = hcloud_server.nextcloud[count.index].id
  network_id = hcloud_network_subnet.hcloud_network_nextcloud_subnet.network_id
}

resource "hcloud_placement_group" "nextcloud_placement_group" {
  name = "nextcloud-placement"
  type = "spread"
  labels = {
    service        = "nextcloud"
    terraform      = true
    placementgroup = true
  }
}

##############################
### FLOATING IP
##############################

resource "hcloud_floating_ip" "nextcloud_floating_ipv4" {
  count     = (length(hcloud_server.nextcloud) >= 2 ? 1 : 0)
  type      = "ipv4"
  server_id = hcloud_server.nextcloud[0].id
}

resource "hcloud_floating_ip" "nextcloud_floating_ipv6" {
  count     = (length(hcloud_server.nextcloud) >= 2 ? 1 : 0)
  type      = "ipv6"
  server_id = hcloud_server.nextcloud[0].id
}

resource "hcloud_rdns" "nextcloud_floating_ipv4_rdns" {
  count          = (length(hcloud_server.nextcloud) >= 2 ? 1 : 0)
  floating_ip_id = hcloud_floating_ip.nextcloud_floating_ipv4[0].id
  ip_address     = hcloud_floating_ip.nextcloud_floating_ipv4[0].ip_address
  dns_ptr        = hcloud_server.nextcloud[count.index].name
}

resource "hcloud_rdns" "nextcloud_floating_ipv6_rdns" {
  count          = (length(hcloud_server.nextcloud) >= 2 ? 1 : 0)
  floating_ip_id = hcloud_floating_ip.nextcloud_floating_ipv6[0].id
  ip_address     = hcloud_floating_ip.nextcloud_floating_ipv6[0].ip_address
  dns_ptr        = hcloud_server.nextcloud[count.index].name
}

##############################
### REVERSE DNS
##############################

resource "hcloud_rdns" "nextcloud_rdns_ipv4" {
  count = length(hcloud_server.nextcloud)

  server_id  = hcloud_server.nextcloud[count.index].id
  ip_address = hcloud_server.nextcloud[count.index].ipv4_address
  dns_ptr    = hcloud_server.nextcloud[count.index].name
}

resource "hcloud_rdns" "nextcloud_rdns_ipv6" {
  count = length(hcloud_server.nextcloud)

  server_id  = hcloud_server.nextcloud[count.index].id
  ip_address = hcloud_server.nextcloud[count.index].ipv6_address
  dns_ptr    = hcloud_server.nextcloud[count.index].name
}

##############################
### DNS
##############################

resource "cloudflare_record" "nextcloud_dns_ipv4" {
  count = length(hcloud_server.nextcloud)

  zone_id = data.cloudflare_zone.dns_zones["goitservers.com"].id
  name    = hcloud_server.nextcloud[count.index].name
  value   = hcloud_server.nextcloud[count.index].ipv4_address
  type    = "A"
  ttl     = 3600
}

resource "cloudflare_record" "nextcloud_dns_ipv6" {
  count = length(hcloud_server.nextcloud)

  zone_id = data.cloudflare_zone.dns_zones["goitservers.com"].id
  name    = hcloud_server.nextcloud[count.index].name
  value   = hcloud_server.nextcloud[count.index].ipv6_address
  type    = "AAAA"
  ttl     = 3600
}

##############################
### HA Address
##############################

resource "cloudflare_record" "nextcloud_ha_ipv4" {
  zone_id = data.cloudflare_zone.dns_zones["goitservers.com"].id
  name    = "nextcloud.goitservers.com"
  value   = (length(hcloud_server.nextcloud) > 1 ? hcloud_floating_ip.nextcloud_floating_ipv4[0].ip_address : hcloud_server.nextcloud[0].ipv4_address)
  type    = "A"
  ttl     = 3600
}

resource "cloudflare_record" "nextcloud_ha_ipv6" {
  zone_id = data.cloudflare_zone.dns_zones["goitservers.com"].id
  name    = "nextcloud.goitservers.com"
  value   = (length(hcloud_server.nextcloud) > 1 ? hcloud_floating_ip.nextcloud_floating_ipv6[0].ip_address : hcloud_server.nextcloud[0].ipv6_address)
  type    = "AAAA"
  ttl     = 3600
}

