
######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

##############################
### Required providers
##############################

terraform {

  cloud {
    organization = "goINPUT"

    workspaces {
      name = "infrastructure-main"
    }
  }

  required_providers {
    acme = {
      source  = "vancluever/acme"
      version = "~> 2.0"
    }

    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.33.2"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.10.0"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.15.0"
    }
  }
}

##############################
### Cloudflare
##############################

provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

data "cloudflare_zone" "dns_zones" {
  for_each = {
    for domain in var.goinput_domains : domain => domain
  }
  name = each.key
}

##############################
### Let's Encrypt
##############################

provider "acme" {
  server_url = var.acme_server_url
}

##############################
### Hetzner
##############################

provider "hcloud" {
  token = var.hetzner_apitoken
}

resource "tls_private_key" "tls_private_key_hetzner_terraform_ed25519" {
  algorithm = "ED25519"
}

resource "hcloud_ssh_key" "hetzner_public_key_terraform" {
  name       = "Terraform Public Key"
  public_key = tls_private_key.tls_private_key_hetzner_terraform_ed25519.public_key_openssh
  labels = {
    type = "terraform"
  }
}

// Maybe change that later and use local data instead.
data "hcloud_ssh_keys" "hetzner_all_keys" {
  depends_on = [
    hcloud_ssh_key.hetzner_public_key_terraform,
    hcloud_ssh_key.hetzner_public_key_users
  ]
}

##############################
### Gitlab
##############################

provider "gitlab" {
  token = var.gitlab_apitoken
}

data "gitlab_project" "gitlab_infra_project" {
  id = var.gitlab_project_id
}

data "gitlab_project_membership" "gitlab_infra_project_members" {
  project_id = var.gitlab_project_id
  inherited  = true
}

output "gitlab_users" {
  value = [
    for member in data.gitlab_project_membership.gitlab_infra_project_members.members : format("%d,%s,%s,%s,%s", member.id, member.username, member.name, member.access_level, member.expires_at)
  ]
}

data "gitlab_user_sshkeys" "gitlab_infra_project_members_ssh_keys" {
  for_each = toset([
    for member in data.gitlab_project_membership.gitlab_infra_project_members.members :
    format("%s", member.username)
  ])
  username = each.value
}

locals {
  ssh_keys = flatten([
    for user, value in data.gitlab_user_sshkeys.gitlab_infra_project_members_ssh_keys :
    [
      for key in value.keys : {
        title = key.title
        key   = key.key
        id    = key.key_id
        user  = user
      }
    ]
  ])
}

resource "hcloud_ssh_key" "hetzner_public_key_users" {
  count = length(local.ssh_keys)

  name       = format("Gitlab Key %s %s %s", local.ssh_keys[count.index].user, local.ssh_keys[count.index].title, "Public Key")
  public_key = local.ssh_keys[count.index].key
  labels = {
    type        = "user_key"
    gitlab_key  = true
    gitlab_user = local.ssh_keys[count.index].user
  }
}


output "ssh_key_data" {
  value = [for key in local.ssh_keys : key]
}
