######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

##############################
### Let's Encrypt
##############################

resource "tls_private_key" "acme_account_private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "acme_registration" "acme_account_registration" {
  account_key_pem = tls_private_key.acme_account_private_key.private_key_pem
  email_address   = var.acme_email
}

resource "tls_private_key" "tls_private_key_private_key" {
  count     = length(var.goinput_domains)
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_cert_request" "tls_cert_request_certification_request" {
  count = length(var.goinput_domains)

  private_key_pem = tls_private_key.tls_private_key_private_key[count.index].private_key_pem
  dns_names = [
    format("%s", var.goinput_domains[count.index]),
    format("*.%s", var.goinput_domains[count.index])
  ]

  subject {
    common_name = format("%s", var.goinput_domains[count.index])
  }
}

resource "acme_certificate" "acme_certificate_certificate" {
  count = length(var.goinput_domains)

  account_key_pem         = acme_registration.acme_account_registration.account_key_pem
  certificate_request_pem = tls_cert_request.tls_cert_request_certification_request[count.index].cert_request_pem

  dns_challenge {
    provider = "cloudflare"

    config = {
      CF_API_EMAIL = var.cloudflare_email
      CF_API_KEY   = var.cloudflare_api_key
    }
  }
}

resource "hcloud_uploaded_certificate" "hetzner_uploaded_certificate" {
  count = length(var.goinput_domains)
  name  = format("%s-wildcard", var.goinput_domains[count.index])

  private_key = tls_private_key.tls_private_key_private_key[count.index].private_key_pem
  certificate = "${acme_certificate.acme_certificate_certificate[count.index].certificate_pem}${acme_certificate.acme_certificate_certificate[count.index].issuer_pem}"

  labels = {
    certificate = var.goinput_domains[count.index]
    wildcard    = true
    terraform   = true
    letsencrypt = true
  }
}
