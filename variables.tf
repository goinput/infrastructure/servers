######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

# Naming: <provider>_<variable_name>

##############################
### Hetzner
##############################

variable "hetzner_apitoken" {
  type      = string
  sensitive = true
}

##############################
### Cloudflare
##############################

variable "cloudflare_email" {
  type      = string
  sensitive = true
}

variable "cloudflare_api_key" {
  type      = string
  sensitive = true
}

##############################
### Gitlab
##############################

variable "gitlab_apitoken" {
  type      = string
  sensitive = true
}

variable "gitlab_project_id" {
  type      = number
  sensitive = false
}

##############################
### Let's Encrypt
##############################

variable "acme_server_url" {
  type      = string
  sensitive = false
}

variable "acme_email" {
  type      = string
  sensitive = false
}

##############################
### goINPUT
##############################

variable "goinput_domains" {
  type      = list(string)
  sensitive = false
}
